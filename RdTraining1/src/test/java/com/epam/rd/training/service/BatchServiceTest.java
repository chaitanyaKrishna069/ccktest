package com.epam.rd.training.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import com.epam.rd.training.custom.exception.NotFoundException;
import com.epam.rd.training.dto.BatchDto;
import com.epam.rd.training.dto.InternDto;
import com.epam.rd.training.model.Batch;
import com.epam.rd.training.model.Intern;
import com.epam.rd.training.repository.BatchRepository;

@ExtendWith(MockitoExtension.class)
class BatchServiceTest {

	@Mock
	private BatchRepository batchRepository;
	
	@Mock
	private ModelMapper modelMapper;
	
	@InjectMocks
	private BatchService batchService;
	
	private Batch batch;
	private BatchDto batchWithInternDto;
	private Intern intern;
	private InternDto internDto;
	
	@BeforeEach
	public void init(){
		batch = new Batch();
		batch.setName("java");
		intern = new Intern();
		intern.setName("cck");
		intern.setEmail("cck@gmail.com");
		intern.setBatch(batch);
		internDto = new InternDto();
		internDto.setName("cck");
		internDto.setEmail("cck@gmail.com");
		List<Intern> interns =  new ArrayList<>();
		List<InternDto> internDtos = new ArrayList<>();
		interns.add(intern);
		internDtos.add(internDto);
		batch.setInterns(interns);
		batchWithInternDto = new BatchDto();
		batchWithInternDto.setName("java");
		batchWithInternDto.setInterns(internDtos);
	}
	
	@Test
	void should_get_All_Batches() {
		List<BatchDto> batchDtos = new ArrayList<>();
		List<Batch> batchs = new ArrayList<>();
		batchDtos.add(batchWithInternDto);
		batchs.add(batch);
		Mockito.when(batchRepository.findAll()).thenReturn(batchs);
		Mockito.when(modelMapper.map(batch, BatchDto.class)).thenReturn(batchWithInternDto);
		batchService.getAll();
		Mockito.verify(batchRepository).findAll();
	}
	
	
	@Test
	void should_getBatch_By_Name() {
		Optional<Batch> optionlBatch = Optional.of(batch);
		Mockito.when(batchRepository.findByName(anyString())).thenReturn(optionlBatch);
		Mockito.when(modelMapper.map(batch, BatchDto.class)).thenReturn(batchWithInternDto);
		batchService.getByName(anyString());
		Mockito.verify(batchRepository).findByName(anyString());
	}
	
	@Test
	void should_ThrowException_getBatchByName() {
		Mockito.when(batchRepository.findByName(anyString())).thenReturn(Optional.empty());
		assertThrows(NotFoundException.class, () -> batchService.getByName("Cck"));
		Mockito.verify(batchRepository).findByName(anyString());
	}
	
	@Test
	void should_CreateBatch() {
		Mockito.when(modelMapper.map(batchWithInternDto, Batch.class)).thenReturn(batch);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		batchService.createWithInterns(batchWithInternDto);
		Mockito.verify(batchRepository).save(batch);
	}
	
	@Test
	void should_updateBatch() {
		Optional<Batch> optionlBatch = Optional.of(batch);
		Mockito.when(batchRepository.findByName(anyString())).thenReturn(optionlBatch);
		Mockito.when(batchRepository.save(batch)).thenReturn(batch);
		batchService.updateWithInters(anyString(), batchWithInternDto);
		Mockito.verify(batchRepository).findByName(anyString());
	}
	
	@Test
	void should_ThrowException_UpdateBatch_With_Upresent_Name() {
		Mockito.when(batchRepository.findByName(anyString())).thenReturn(Optional.empty());
		assertThrows(NotFoundException.class, () -> batchService.updateWithInters("Cck", batchWithInternDto));
		Mockito.verify(batchRepository).findByName(anyString());
	}
	
	@Test
	void should_Delete_BatchByName() {
		Optional<Batch> optionalBatch = Optional.of(batch);
		Mockito.when(batchRepository.findByName(anyString())).thenReturn(optionalBatch);
		batchService.delete(anyString());
		Mockito.verify(batchRepository).findByName(anyString());
	}
	
	@Test
	void should_ThrowException_Delete_BatchBy_UnpresentName() {
		Mockito.when(batchRepository.findByName(anyString())).thenReturn(Optional.empty());
		assertThrows(NotFoundException.class, () -> batchService.delete("Cck"));
		Mockito.verify(batchRepository).findByName(anyString());
	}
}
