package com.epam.rd.training.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.rd.training.dto.BatchDto;
import com.epam.rd.training.service.BatchService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(BatchController.class)
public class BatchControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	private BatchService batchService;

	@Test
	void should_Get_Batches() throws Exception {
		BatchDto batch = new BatchDto();
		List<BatchDto> batchDtos = new ArrayList<>();
		batchDtos.add(batch);
		Mockito.when(batchService.getAll()).thenReturn(batchDtos);
		mockMvc.perform(get("/batches/interns")).andExpect(status().isOk());
	}

	@Test
	void should_Create_Batches() throws Exception {
		BatchDto batchDto = new BatchDto();
		Mockito.when(batchService.createWithInterns(batchDto)).thenReturn(batchDto);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonForm = objectMapper.writeValueAsString(batchDto);
		mockMvc.perform(post("/batches/interns").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(status().isCreated());
	}

	@Test
	void should_Update_Batches() throws Exception {
		BatchDto batchDto = new BatchDto();
		Mockito.when(batchService.updateWithInters("cck", batchDto)).thenReturn(batchDto);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonForm = objectMapper.writeValueAsString(batchDto);
		mockMvc.perform(
				put("/batches/interns").param("name", "cck").contentType(MediaType.APPLICATION_JSON).content(jsonForm))
				.andExpect(status().isOk());
	}

	@Test
	void should_Delete_Batches() throws Exception {
		Mockito.doNothing().when(batchService).delete("cck");
		mockMvc.perform(delete("/batches/interns").param("name", "cck")).andExpect(status().isOk());
	}
}
