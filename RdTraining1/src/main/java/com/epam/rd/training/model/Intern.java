package com.epam.rd.training.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Intern {
	@Id
	@GeneratedValue
	private int id;
	private String name;
	@Column(unique = true)
	private String email;
	@ManyToOne
	@JoinColumn(name = "batch_id")
	private Batch batch;
}