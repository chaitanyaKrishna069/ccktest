package com.epam.rd.training.custom.exception;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.epam.rd.training.utility.ExceptionResponse;

import jakarta.validation.ConstraintViolationException;

@RestControllerAdvice(basePackages = "com.epam.rd.training.controller")
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendArgumentInvalidResponse(MethodArgumentNotValidException exception,
			WebRequest request) {
		List<String> errors = new ArrayList<>();
		exception.getAllErrors().forEach(error -> errors.add(error.getDefaultMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), errors.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendConstraintInvalidResponse(ConstraintViolationException exception, WebRequest request) {
		List<String> errors = new ArrayList<>();
		exception.getConstraintViolations().forEach(error -> errors.add(error.getMessage()));
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), errors.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendNotFoundEntryResponse(NotFoundException exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), exception.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(SQLIntegrityConstraintViolationException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ExceptionResponse sendIntegrityExcpetionResponse(SQLIntegrityConstraintViolationException exception,
			WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.BAD_REQUEST.name(), exception.toString(),
				request.getDescription(false));
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public ExceptionResponse sendInternalServerErrorIssue(Exception exception, WebRequest request) {
		return new ExceptionResponse(new Date().toString(), HttpStatus.INTERNAL_SERVER_ERROR.name(),
				exception.getMessage(), request.getDescription(false));
	}
}