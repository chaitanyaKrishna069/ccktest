package com.epam.rd.training.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.rd.training.model.Intern;

public interface InternRepository extends JpaRepository<Intern, Integer> {
	public Optional<Intern> findByEmail(String email);
}
