package com.epam.rd.training.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.rd.training.custom.exception.NotFoundException;
import com.epam.rd.training.dto.InternDto;
import com.epam.rd.training.model.Intern;
import com.epam.rd.training.repository.InternRepository;

@Service
public class InternService {
	
	@Autowired
	ModelMapper modelMapper;
	
	@Autowired
	InternRepository internRepository;
	
	public List<InternDto> getAll(){
		return internRepository.findAll().stream().map(intern -> modelMapper.map(intern, InternDto.class)).toList();
	}
	
	public void delete(String email) {
		Intern intern = internRepository.findByEmail(email).orElseThrow(
				() -> {throw new NotFoundException("The given Mail Id dosen't Exists");});
		internRepository.delete(intern);
	}
}
