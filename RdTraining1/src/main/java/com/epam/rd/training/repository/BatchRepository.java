package com.epam.rd.training.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.rd.training.model.Batch;

public interface BatchRepository extends JpaRepository<Batch, Integer>{
	public boolean existsByName(String name);
	public Optional<Batch> findByName(String name);
}
