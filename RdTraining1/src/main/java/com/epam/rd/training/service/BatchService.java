package com.epam.rd.training.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.rd.training.custom.exception.NotFoundException;
import com.epam.rd.training.dto.BatchDto;
import com.epam.rd.training.model.Batch;
import com.epam.rd.training.repository.BatchRepository;

@Service
public class BatchService {

	@Autowired
	public BatchRepository batchRepository;

	@Autowired
	public ModelMapper modelMapper;

	public List<BatchDto> getAll() {
		return batchRepository.findAll().stream().map(batch -> modelMapper.map(batch, BatchDto.class))
				.toList();
	}

	public BatchDto getByName(String name) {
		Batch batch = batchRepository.findByName(name).orElseThrow(() -> {
			throw new NotFoundException("The batch Name dosen't exists");
		});
		return modelMapper.map(batch, BatchDto.class);
	}

	public BatchDto createWithInterns(BatchDto batchWithInternDto) {
		Batch batch = modelMapper.map(batchWithInternDto, Batch.class);
		return modelMapper.map(batchRepository.save(batch), BatchDto.class);
	}

	public BatchDto updateWithInters(String name, BatchDto batchWithInternDto) {
		Batch batch = batchRepository.findByName(name).orElseThrow(() -> {
			throw new NotFoundException("The Batch name doest't exists");
		});
		modelMapper.map(batchWithInternDto, batch);
		batchRepository.save(batch);
		return modelMapper.map(batch, BatchDto.class);
	}

	public void delete(String name) {
		Batch batch = batchRepository.findByName(name).orElseThrow(() -> {
			throw new NotFoundException("The Batch name doest't exists");
		});
		batchRepository.delete(batch);
	}
}