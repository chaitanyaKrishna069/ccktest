package com.epam.rd.training.model;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor
public class Batch {
	@Id
	@GeneratedValue
	private int id;
	@Column(unique = true)
	private String name;
	@OneToMany(mappedBy = "batch", cascade = CascadeType.ALL)
	private List<Intern> interns;

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setInterns(List<Intern> interns) {
		interns.forEach(intern -> intern.setBatch(this));
		this.interns = interns;
	}
}