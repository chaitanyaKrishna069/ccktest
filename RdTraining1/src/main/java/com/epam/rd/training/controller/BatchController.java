package com.epam.rd.training.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.rd.training.dto.BatchDto;
import com.epam.rd.training.service.BatchService;

@RestController
public class BatchController {
	@Autowired
	public BatchService batchService;
	
	@GetMapping("/batches/interns")
	public ResponseEntity<List<BatchDto>> getAll(){
		return new ResponseEntity<>(batchService.getAll() , HttpStatus.OK);
	}
	
	@PostMapping("/batches/interns")
	public ResponseEntity<BatchDto> createBatchWithInterns(@RequestBody BatchDto batch) {
		return new ResponseEntity<>(batchService.createWithInterns(batch), HttpStatus.CREATED);
	}
	
	@PutMapping("/batches/interns")
	public BatchDto updateBatchWithInterns(@RequestParam String name, @RequestBody BatchDto batch) {
		return batchService.updateWithInters(name,batch);
	}
	
	@DeleteMapping("/batches/interns")
	public void deleteBatchWithInterns(@RequestParam String name) {
		batchService.delete(name);
	}
}