package com.epam.rd.training.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class InternDto {
	private String name;
	private String email;
}
